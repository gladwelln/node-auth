Node.js, Express and MySQL
=====================

**A basic Auth and user's CRUD developed in Node.js, Express and MySQL.

## Requirements

* Node      :   https://nodejs.org/en/docs/
* Express   :   https://www.npmjs.com/package/express
* ejs       :   https://www.npmjs.com/package/ejs
* MySQL     :   https://www.npmjs.com/package/mysql

## Installation
git clone https://gladwelln@bitbucket.org/gladwelln/node-auth.git app_name

cd /path/to/app_name

npm install

## Configuration (database)
open /path/to/app_name/app.js and update below database connection details:

        host: 'localhost',
        user: 'root',
        password : 'root',
        port : 3306
        database:'node-auth'

You'll also need to create a DB named 'node-auth' and import /path/to/app_name/node-auth.sql

Then start the application:
    > cd /path/to/app_name
    > npm run watch