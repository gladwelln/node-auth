var userListData = [];

$(document).ready(function()
{
    //Get user list - Store into a global array
    getUserList();

    //Show clicked user information
    $('#userList table tbody').on('click', 'td button.showuser', showUserInfo);

    //Delete user
    $('#userList table tbody').on('click', 'td button.deleteuser', deleteUser);

    //Default layout configurations
    configLayout();
});

jQuery.extend({
    doAJAX: function (url, data, type, callback)
    {
        type = (type) ? type : "GET";

        return $.ajax({
            type: type,
            url: baseUrl + url,
            data: data,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert("Error Code: " + XMLHttpRequest.status + " : " + XMLHttpRequest.statusText);
            },
            success: function (data)
            {
                //Handle session expire.
                if(data.status == false && data.error_description == 'Your session has expired')
                {
                    var results = confirm(data.error_description + ", would you like to login?");
                    if(results)
                    {
                        /*
                            Reload the page - this will auto redirect to login: see routes/auth.js isAuthenticated()
                            We could also use Jquery to redirect: window.location.replace(baseUrl);
                        */

                        window.location.reload();
                        return false;
                    }
                }

                callback(data);
            }
        });
    }
});

function configLayout()
{
    $('html, body').css({
        overflow: 'hidden',
        height: '100%'
    });

    $('.content-wrapper').find('.list-group').height($(window).height() - 120);
    $('.content-wrapper').find('.list-group').css('overflow-x', 'hidden');
}

var Dashboard = function() {
	var global = {
		tooltipOptions: {
			placement: "right"
		},
		menuClass: ".c-menu"
	};

    /*var menuChangeActive = function menuChangeActive(el) 
    {
		var hasSubmenu = $(el).hasClass("has-submenu");
		$(global.menuClass + " .is-active").removeClass("is-active");
		$(el).addClass("is-active");

        // if (hasSubmenu) 
        //{
		// 	$(el).find("ul").slideDown();
		//}
    };*/

    var sidebarChangeWidth = function sidebarChangeWidth() 
    {
		var $menuItemsTitle = $("li .menu-item__title");

		$("body").toggleClass("sidebar-is-reduced sidebar-is-expanded");
		$(".hamburger-toggle").toggleClass("is-opened");

        if ($("body").hasClass("sidebar-is-expanded"))
        {
			$('[data-toggle="tooltip"]').tooltip("destroy");
        }
        else
        {
			$('[data-toggle="tooltip"]').tooltip(global.tooltipOptions);
		}
	};

    return {
        init: function init()
        {
			$(".js-hamburger").on("click", sidebarChangeWidth);

			/*$(".js-menu li").on("click", function (e) {
				menuChangeActive(e.currentTarget);
			});*/

			$('[data-toggle="tooltip"]').tooltip(global.tooltipOptions);
		}
	};
}();

Dashboard.init();

function getUserList()
{
    $.doAJAX('/userlist', {}, 'GET', function( response )
    {
        if(response.status == true)
        {
            userListData = response.content;

            //Count users in a global array and display on the front-end
            update_user_count(userListData);

            //Populate the user table
            populateTable(userListData);
        }
        else
        {
            alert(response.error_description);
        }
    });

    return false;
}

function populateTable(data)
{
    var html = '';
    data = (userListData) ? userListData : data;

    $.each(data, function()
    {
        html += '<tr>';
        html += '<td>' + this.username + '<a/></td>';
        html += '<td>' + this.fname + '</td>';
        html += '<td>' + this.lname + '</td>';
        html += '<td>' + this.created_at + '</td>';
        //html += '<td>' + this.updated_at + '</td>';
        html += '<td>';
        html += '<div class="btn-group btn-group-justified" role="group" aria-label="...">';
        html += '<div class="btn-group" role="group">';
        html += '<button type="button" class="btn btn-success btn-sm showuser" userid="' + this.id + '">View</button>';
        html += '</div>';
        html += '<div class="btn-group" role="group">';
        html += '<button type="button" class="btn btn-default btn-sm" onclick="showUserForm(' + this.id + ')">Edit</button>';
        html += '</div>';
        html += '<div class="btn-group" role="group">';
        html += '<button type="button" class="btn btn-danger btn-sm deleteuser" userid="' + this.id + '">Delete</button>';
        html += '</div>';
        html += '</div>';
        html += '</td>';
        html += '</tr>';
    });

    $('#userList table tbody').html(html);
}

function getUserDetails(userid)
{
    var arrayPosition = userListData.findIndex(arrayItem => arrayItem.id == userid);

    return (userListData[arrayPosition]) ? userListData[arrayPosition] : [];
}

//Show User Info
function showUserInfo(event)
{
    // Retrieve user id from link userid attribute
    var userid = $(this).attr('userid');

    // Get Index of object based on id value
    //var arrayPosition = userListData.findIndex(arrayItem => arrayItem.id == userid);

    // Get our User Object
    var thisUserObject = getUserDetails(userid);

    var html;
    html ='<div class="modal-header">';
    html +='<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    html +='<h4 class="modal-title">User Details</h4>';
    html +='</div>';
    html +='<div class="modal-body">';
    html +='<ul class="list-group">';
    html +='<li class="list-group-item">';
    html +='<div class="row">';
    html +='<div class="col-md-6 text-info">User ID</div>';
    html +='<div class="col-md-6">' + thisUserObject.id + '</div>';
    html +='</div>';
    html +='</li>';
    html +='<li class="list-group-item">';
    html +='<div class="row">';
    html +='<div class="col-md-6 text-info">Username</div>';
    html +='<div class="col-md-6">' + thisUserObject.username + '</div>';
    html +='</div>';
    html +='</li>';
    html +='<li class="list-group-item">';
    html +='<div class="row">';
    html +='<div class="col-md-6 text-info">Full Name</div>';
    html +='<div class="col-md-6">' + thisUserObject.fname + ' ' + thisUserObject.lname + '</div>';
    html +='</div>';
    html +='</li>';
    html +='<li class="list-group-item">';
    html +='<div class="row">';
    html +='<div class="col-md-6 text-info">Join Date</div>';
    html +='<div class="col-md-6">' + thisUserObject.created_at + '</div>';
    html +='</div>';
    html +='</li>';
    html +='<li class="list-group-item">';
    html +='<div class="row">';
    html +='<div class="col-md-6 text-info">Last Updated at</div>';
    html +='<div class="col-md-6">' + thisUserObject.updated_at + '</div>';
    html +='</div>';
    html +='</li>';
    html +='</ul>';
    html +='</div>';
    /*html +='<div class="modal-footer">';
    html +='<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
    html +='<button type="button" class="btn btn-primary">Save changes</button>';
    html +='</div>';*/

    //alert(thisUserObject.fname);
    $('#global-modal .modal-content').html(html);
    $('#global-modal').modal();
};

//Show user form
function showUserForm(userid)
{
    var title = "New User Details";
    var userdetails = {};
    var readOnly = "";
    if(userid)
    {
        // Get our User Object
        userdetails = getUserDetails(userid);
        title = userdetails.username + " Details";
        readOnly = 'readonly';
    }

    var html;
    html ='<div class="modal-header">';
    html +='<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    html +='<h4 class="modal-title">' + title + '</h4>';
    html +='</div>';
    html +='<div class="modal-body">';
    
    html +='<form class="form-horizontal" id="user-form">';
    
    if(userid) { html +='<input type="hidden" id="userid" value="' + userid + '">'; }

    html +='<div class="form-group">';
    html +='<label for="fname" class="col-sm-3 control-label">First Name</label>';
    html +='<div class="col-sm-9">';
    html +='<input type="text" class="form-control" id="fname" value="' + ((typeof userdetails.fname != 'undefined') ? userdetails.fname : '') + '">';
    html +='</div>';
    html +='</div>';
    html +='<div class="form-group">';
    html +='<label for="lname" class="col-sm-3 control-label">Last Name</label>';
    html +='<div class="col-sm-9">';
    html +='<input type="text" class="form-control" id="lname" value="' + ((typeof userdetails.lname != 'undefined') ? userdetails.lname : '') + '">';
    html +='</div>';
    html +='</div>';
    html +='<div class="form-group">';
    html +='<label for="gender" class="col-sm-3 control-label">Gender</label>';
    html +='<div class="col-sm-9">';
    html +='<select class="form-control" id="gender" ' + readOnly + '>';
    html +='<option value="Male" ' + ((typeof userdetails.gender != 'undefined' && userdetails.gender == 'Male') ? 'selected' : '') + '>Male</option>';
    html +='<option value="Female" ' + ((typeof userdetails.gender != 'undefined' && userdetails.gender == 'Female') ? 'selected' : '') + '>Female</option>';
    html +='</select>';
    html +='</div>';
    html +='</div>';
    html +='<div class="form-group">';
    html +='<label for="username" class="col-sm-3 control-label">Username</label>';
    html +='<div class="col-sm-9">';
    html +='<input type="text" class="form-control" id="username" value="' + ((typeof userdetails.username != 'undefined') ? userdetails.username : '') + '" ' + readOnly + '>';
    html +='</div>';
    html +='</div>';
    html +='<div class="form-group">';
    html +='<label for="password" class="col-sm-3 control-label">Password</label>';
    html +='<div class="col-sm-9">';
    html +='<input type="password" class="form-control" id="password">';
    html +='</div>';
    html +='</div>';
    html +='<div class="form-group">';
    html +='<label for="password_confirm" class="col-sm-3 control-label">Re-Password</label>';
    html +='<div class="col-sm-9">';
    html +='<input type="password" class="form-control" id="password_confirm">';
    html +='</div>';
    html +='</div>';
    html +='</form>';

    html +='</div>';
    html +='<div class="modal-footer">';
    html +='<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>';
    html +='<button type="button" class="btn btn-success pull-right" onclick="saveUserDetails();">Save</button>';
    html +='</div>';

    //alert(thisUserObject.fname);
    $('#global-modal .modal-content').html(html);
    $('#global-modal').modal();
}

//Add / Edit User
function saveUserDetails() 
{
    var errors = '';
    var userData = {};

    $('form#user-form .form-control').each(function(i) 
    {
        if($(this).val())
        {
            userData[$(this).attr('id')] = $(this).val();
        }
        else
        {
            errors += "Provide a value for " + $(this).attr('id') + "\n";
        }
    });

    /*Check password match*/
    if(userData['password'] != userData['password_confirm'])
    {
        errors += "Passwords do not match.";
    }

    if(errors.length) 
    {
        alert(errors);

        return false;
    }

    //We do not need password confirm key
    delete userData['password_confirm'];

    //If userid input exist, do update else do add
    var userid = ($('input#userid').length) ? $('input#userid').val() : '';
    var action = (userid) ? '/edituser/' + userid : '/adduser';
    var method = (userid) ? 'PUT' : 'POST';

    $.doAJAX(action, userData, method, function( response )
    {
        if(response.status == true)
        {
            $('#global-modal').modal('hide');

            //Update the table
            getUserList();
        }
        else
        {
            alert(response.error_description);
        }
    });
};

//Delete User
function deleteUser() 
{
    // Pop up a confirmation dialog
    var confirmation = confirm('Are you sure you want to delete this user?');

    // Check and make sure the user confirmed
    if (confirmation === true) 
    {
        // If they did, do our delete
        $.doAJAX('/deleteuser/' + $(this).attr('userid'), {}, 'DELETE', function( response )
        {
            if(response.status == true)
            {
                // Update the table
                getUserList();
            }
            else
            {
                alert(response.error_description);
            }
        });
    }
    else 
    {
        // If they said no to the confirm, do nothing
        return false;
    }
};

function update_user_count(userListData)
{
    userListData = (userListData) ? userListData : userListData;
    
    $('span.user-count').text(userListData.length);
}