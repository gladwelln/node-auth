
exports.dashboard = function(req, res, next)
{
    var user = (req.session) ? req.session.user : null;
    if(user == null)
    {
        res.redirect("/");
        return;
    }
            
    res.render('./pages/users/dashboard', { user: user });
};

exports.userlist = function(req, res, next)
{
    var user_id = req.session.user.id;
    db.query('SELECT * FROM user WHERE id != ?', user_id, function (err, records, fields) 
    {
        var results = (err) ? { status: false, error_description: err.sqlMessage } : { status: true, content: records };

        res.json(results);
    });
};

exports.deleteuser = function(req, res)
{
    var user_id = req.params.id;
    db.query('DELETE FROM user WHERE id = ?', user_id, function(err, result)
    {    
        var results = (err) ? { status: false, error_description: err.sqlMessage } : { status: true };

        res.json(results);
    }); 
};

exports.adduser = function(req, res)
{
    //Check if username is exist
    is_taken(req.body.username, function(taken)
    {
        if(taken)
        {
            res.json({ status: false, error_description: 'Username taken, please try again!' });
        }
        else
        {
            db.query('select MD5("' + req.body.password + '") as "md5_password"', function (err, record) 
            {
                req.body.password = record[0].md5_password;

                db.query('INSERT INTO user SET ?', req.body, function(err, result) 
                {
                    var results = (err) ? { status: false, error_description: err.sqlMessage } : { status: true };

                    res.json(results);
                });
            });
        }
    });
};

exports.edituser = function(req, res)
{
    var user_id = req.params.id;
    var fname = req.body.fname;
    var lname = req.body.lname;
    //var gender = req.body.gender;
    //var username = req.body.username;
    var password = req.body.password;
    var stm = 'UPDATE user SET fname = "' + fname + '", lname = "' + lname + '", password = MD5("' + password + '"), updated_at = NOW() WHERE id = ?';

    db.query(stm, user_id, function(err, result) 
    {    
        var results = (err) ? { status: false, error_description: err.sqlMessage } : { status: true };

        res.json(results);
    }); 
};

function is_taken(username, callback)
{
    db.query('SELECT * FROM user WHERE username = ?', username, function (err, records) 
    {
        var taken = false;

        if(records.length)
        {
            taken = true;
        }

        return callback(taken);
    });
}