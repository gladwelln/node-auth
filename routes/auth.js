exports.login = function(req, res)
{
    var sess = req.session; 
    
    if(req.method == "POST")
    {
        var post = req.body;
        var username = post.username;
        var password = post.password;
      
        var sql = "SELECT * FROM `user` WHERE `username`='" + username + "' AND password = MD5('" + password + "')";
        db.query(sql, function(err, results)
        {
            if(err)
            {
                res.render('./pages/auth/login', { message: 'Mysql error: ' + err.sqlMessage });
                return;
            }

            if(results.length)
            {
                //Store user details to a session
                //var hour = 3600000;
                sess.user = results[0];
                //sess.cookie.expires = new Date(Date.now() + hour);
                //sess.cookie.maxAge = hour;

                res.redirect('/dashboard');
            }
            else
            {
                res.render('./pages/auth/login', { message: 'Invalid Credentials, please try again!' });
            }
       });
    } 
    else 
    {
        res.render('./pages/auth/login', { message: '' });
    }         
};

exports.register = function(req, res)
{
    if(req.method == "POST")
    {
        var post  = req.body;
        var username = post.username;
        var pass = post.password;
        var pass_conf = post.password_conf;
       
        if(pass_conf != pass)
        {
            res.render('./pages/auth/register', { message: 'Your password does not match!' });
            return;
        }
        
        //Check if username is exist
        is_taken(username, res, function(taken)
        {
            if(taken)
            {
                res.render('./pages/auth/register', { message: 'Username taken, please try again!' });
            }
            else
            {
                var sql = "INSERT INTO `user`(`username`, `password`) VALUES ('" + username + "', MD5('" + pass + "'))";
                var query = db.query(sql, function(err, result)
                {
                    res.render('./pages/auth/login', { message: 'Your account has been created, you may now login!' });
                });
            }
        });
    } 
    else 
    {
       res.render('./pages/auth/register', { message: '' });
    }
};

exports.profile = function(req, res)
{
    if(!req.session.user)
    {
        res.redirect("/");
        return;
    }
    
    if(req.method == "POST")
    {
        
    }
    else
    {
        res.render('./pages/auth/profile', { user: req.session.user });
    }
};

exports.logout = function(req, res)
{
    req.session.destroy();
    res.redirect('/');
};

//Auth Middleware
exports.isAuthenticated = function(req, res, next)
{
    var url = (req.originalUrl).replace('/', '');
    var baseUrl = req.protocol + '://' + req.get('host');
    
    if(!global.url || !global.baseUrl)
    {
        global.url = url;
        global.baseUrl = baseUrl;
    }
    
    if (req.session.user)
    {
        //Check if trying to access auth page
        if(!url.length || url == 'login' || url == 'register')
        {
            //Redirect them to dashboard
            res.redirect('/dashboard');
            return;
        }

        return next();
    }
  
    if(!url.length || url == 'login' || url == 'register')
    {
        return next();
    }
    
    //IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM TO LOGIN
    if(req.xhr)
    {
        res.json({ status: false, error_description: 'Your session has expired' });
    }
    else
    {
        res.redirect('/login');
    }
}

function is_taken(username, res, callback)
{
    db.query('SELECT * FROM user WHERE username = ?', username, function (err, records) 
    {
        if(err)
        {
            res.render('./pages/auth/register', { message: 'Mysql error: ' + err.sqlMessage });
            return;
        }
        
        var taken = false;
        if(records.length)
        {
            taken = true;
        }

        return callback(taken);
    });
}