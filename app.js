var express = require('express')
  , auth = require('./routes/auth')
  , users = require('./routes/users')
  , http = require('http')
  , path = require('path')
  , session = require('express-session')
  , mysql = require('mysql')
  , bodyParser = require("body-parser")
  , ejs = require("ejs-locals");

var app = express();

//DB Configurations
var connection = mysql.createPool({
                  host     : 'localhost',
                  user     : 'root',
                  password : '',
                  database : 'node-auth'
                });

/*connection.connect(function(err) 
{
  if(err)
  {
    console.log(err.sqlMessage);
  }
});*/

//Set connection info, this will auto connect on db.query execution
global.db = connection;

//Initialize session
app.use(session({
  secret: 'a4f8071f-c873-4447-8ee2',
  resave: false,
  saveUninitialized: true,
  // cookie: { maxAge: 60000 }
}));

//all environments
//app.set('port', process.env.PORT || 5000);
app.set('views', path.join(__dirname + '/views'));
app.engine('ejs', ejs);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.get('/', auth.isAuthenticated, auth.login);
app.get('/login', auth.isAuthenticated, auth.login);
app.post('/login', auth.login);
app.get('/register', auth.isAuthenticated, auth.register);
app.post('/register', auth.register);
app.get('/dashboard', auth.isAuthenticated, users.dashboard);
app.get('/userlist', auth.isAuthenticated, users.userlist);
app.post('/adduser', auth.isAuthenticated, users.adduser);
app.put('/edituser/:id', auth.isAuthenticated, users.edituser);
app.delete('/deleteuser/:id', auth.isAuthenticated, users.deleteuser);
app.get('/profile', auth.isAuthenticated, auth.profile);
app.get('/logout', auth.logout);

var server = app.listen(5000, function ()
{
  var host = server.address().address;
  var port = server.address().port;

  console.log('Node-auth app listening at http://%s:%s', host, port);
});